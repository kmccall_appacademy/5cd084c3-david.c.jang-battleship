class Board
  attr_reader :grid

  def initialize(grid=Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def display
    p @grid
  end

  def count
    count = 0
    @grid.each{ |array| array.each { |space| count += 1 if space == :s} }
    count
  end

  def empty?(position= false)
    # position passed in
    return @grid[position[0]][position[1]].nil? if position
    # no position passed in
    @grid.none? { |array| array.all? { |space| space == :s}}

  end
  # randomly distribute ships on the board
  def place_random_ship
    if full?
      raise "Board is full"
    else
      empty_indices = []
      @grid.each_with_index { |array, indx| array.each_with_index { |position, indx2|
               empty_indices << [indx, indx2] if @grid[indx][indx2].nil?}}
      random = empty_indices.sample
      @grid[random[0]][random[1]] = :s
    end
  end

  def in_range?(position)
    position.all? { |x| x.between?(0, @grid.length - 1) }
  end

  def full?
    @grid.all? {|array| array.all? { |position| !position.nil?}}
  end

  def won?
    empty?
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    @grid[row][col] = val
  end
end
