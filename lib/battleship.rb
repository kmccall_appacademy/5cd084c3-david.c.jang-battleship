class BattleshipGame
  attr_reader :board, :player
  def initialize(player=HumanPlayer.new("David"), board=Board.random)
    @player = player
    @board = board
    # used to keep track of previous attack
    @last_attack = false
  end

  def play
    play_turn until game_over?
    declare_winner
  end

  def count
    @board.count
  end

  def play_turn
    pos = nil

   until valid?(pos)
     display_status
     pos = player.get_play
   end

   attack(pos)

  end

  def valid?(pos)
    pos.is_a?(Array) && board.in_range?(pos)
  end

  def previous_hit?
    @last_attack
  end

  def attack(position)
    if @board[position] == :s
      @last_attack = true
    else
      @last_attack = false
    end

    board[position] = :x
  end

  def game_over?
    @board.won?
  end

  def display_status
    system("clear")
    board.display
    puts "It's a hit!" if previous_hit?
    puts "There are #{count} ships remaining."
  end


end
